# Responsive Design Code Challenge Overview

Using one of the popular front end frameworks (Backbone, Angular, React, Ember), create a single web page from the attached PSD using HTML, CSS, and JavaScript.

**Deliverables**

All files used should be enclosed in zip file so they can be extracted and viewed locally on
any web browser.

**Requirements**

  - Only vanilla JS, jQuery and necessary framework base libraries should be used. No
third-party JS plugins or libraries.
  - The attached data.txt file should be consumed by the front end to create the page
navigation.
  - At full desktop size, the web page should match the PSD as precisely as possible.
  - The max content width is defined by the PSD. When over max width the content
should be centered in the browser. (Hero images may still extend to full viewport
width.)
  - The navigation should use conventional styles for small screens when the site
collapses responsively.
  - Some suggestions for interactive hover states exist in the PSD layers.
Evaluation
The page should demonstrate your best skills and creativity in the following areas:
  - Framework Knowledge: The JS should reflect the breadth of your understanding of
the chosen JS framework and its best practices.
  - Responsive Design : The page should be fluidly responsive using a responsive
framework of your choosing. Add screen size breakpoints at your discretion. Use best
judgement for order and placement of page elements when site collapses.
  - Interactive Experience : The UI should provide an interactive experience, utilizing
CSS transitions/animations and JavaScript interactions at your discretion. Ensure
interactions gracefully degrade on touch devices.
  - Parallax Scrolling : Add parallax scrolling to enhance the page

---
#### Design

![challenge design](https://cldup.com/LP1jrUJWr0.thumb.png)

---

### Setup
To set up the project you need to have Node and NPM installed.
I've included a standard package.json file created through npm. To set up the project and install all needed dependencies run:

`$ npm install`

I've also included a really small node server I use for front end app development.
To start the server run after the above npm install run:

`$ http-server`

Then navigate in your browser to:
[http://localhost:8080](http://localhost:8080)

### Frameworks / Tools

#### Javascript
In order to maximize feature development time I went with Angular as it is the most familiar.

I utilized the core angular router `ng-route` and built some "views" out under the about the crew menu item. All routes are defined in:
`/app/routes.js`

#### Sass

I've added [bourbon](http://bourbon.io) For browser prefixing and handy access to css3 niceties. The remainder is a pretty light collection of partials and app specific styles all whipped up from scratch.

##### Responsive behaviors

Media queries are defined via a light set of mix-ins that follow a mobile first approach.

```
@include tablet { > 600px
  ...
}
@include desktop { > 1024px
  ...
}
```

*ref:* `/assets/sass/_partials/_responsive.scss`

### Data

I've tried to rely on most data being stored as an object in my services and out of the app controller. I find this helps keep things clean if the controller simply works as a dispatcher to bind data into scope and call service methods. It also decouples the data for each feature from the app scope allowing for quick updates or enhancements.

*note* - `data.txt` I converted this to `.json` and took liberties in using it for mock data and to demonstrate http interactions. Sorry if there was intentionality in using txt format.

### TODO

So I had a few more items that I wanted to really blow out but I ended up running out of time.

- Fully responsive mobile menu, it's really close just not complete
- Production build tasks ( concat and minify )
- More native css / js animations and transitions I went for no jQuery to push myself and suffered some quick wins in this department.
- More views. I had so much fun given the excellent choice of subject matter that I would have liked to have built more features.
- More testing. I've done some initial testing but hardly enough to consider it production ready.

Overall I really enjoyed the challenge and hope to receive some feedback. If there are any questions please feel free to reach out.
