'use strict';

angular.module('bsgApp', ['routes'])

.factory('_', function() {
  return window._;
})

.controller('appCtrl', function(navService, footerService) {

  // load globally needed data
  navService.loadNavigation();
  footerService.init();
})

.controller('crewCtrl', function($scope, crewService) {
  $scope.member = crewService.data.member;
})
