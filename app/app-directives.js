'use strict';

angular.module('bsgApp')

.constant('TMPL', '/app/templates/')

.directive('appHeader', function(navService, TMPL) {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: TMPL + 'app-header.html',
    link: function(scope, elem, attr) {
      scope.navData = navService.data;

      scope.toggleNav = function() {
        return navService.toggle();
      };

      scope.subToggle = function() {
        return navService.subToggle();
      };
    }
  }
})

.directive('appFooter', function(TMPL, footerService) {
  return {
    restict: 'E',
    replace: true,
    templateUrl: TMPL + 'app-footer.html',
    link: function(scope) {
      scope.footer = footerService.data;
    }
  }
})

.directive('primaryHero', function(TMPL) {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: TMPL + 'primary-hero.html',
    link: function(scope) {
    }
  }
})

.directive('videoCallout', function(TMPL) {
  return {
    restict: 'E',
    replace: true,
    templateUrl: TMPL + 'video-callout.html',
    link: function(scope) {

    }
  }
})
