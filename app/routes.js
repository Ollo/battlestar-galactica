'use strict';

angular.module('routes', ['ngRoute'])

.constant('PATH', '/app/templates/')

.config(function($routeProvider, $locationProvider, PATH) {

  $routeProvider
    .when('/', {
      templateUrl: PATH + 'home-page.html',
      controller: 'appCtrl',
      resolve: {
      }
    })
    .when('/crew/:crewId', {
      templateUrl: PATH + 'crew-member.html',
      controller: 'crewCtrl',
      resolve: {
        crewData: function(crewService, $routeParams, $route) {
          var crewId = $route.current.params.crewId;
          return crewService.getCrewById(crewId);
        }
      }
    })
    .when('/ship/:moduleId', {
      templateUrl: PATH + 'ship-module.html',
      resolve: {}
    })
    .otherwise({
        redirectTo: '/',
    });

  // configure html5 to get links working on jsfiddle
  $locationProvider.html5Mode(true);
});
