'use strict';

angular.module('bsgApp')

.constant('API', '/app/api/data.json')

.service('navService', function(API, $http, $q) {

  var self = this;
  self.data = {
    navItems: []
  };

  self.loadNavigation = function() {
    return $http.get(API)
    .then(function(resp) {
      return self.data.navItems = resp.data.navigation;
    });
  };

  self.toggle = function() {
    // debugger;
    var menu = angular.element(document.querySelector('.mobile_navigation-menu'));
    menu.toggleClass('open');
  };

  self.subToggle = function() {
    var menu = angular.element(document.querySelector('.mobile_subnav-menu'));
    menu.toggleClass('open');
  };
})

.service('footerService', function(API, $http, $q) {
  var self = this;
  self.data = {
    news: [],
    events: [],
    featured: {}
  };

  self.getNews = function() {
    return $http.get(API)
    .then(function(resp) {
      return self.data.news = resp.data.news;
    })
  };

  self.getEvents = function() {
    return $http.get(API)
    .then(function(resp) {
      return self.data.events = resp.data.events;
    })
  };

  self.getFeatured = function() {
    return $http.get(API)
    .then(function(resp) {
      return self.data.featured = resp.data.featured;
    })
  };

  self.init = function() {
    self.getNews();
    self.getEvents();
    self.getFeatured();
    return self;
  };

})

.service('crewService', function(API, $http, $q, _) {
  var self = this;
  self.data = {
    member: {}
  }

  self.getCrewById = function(crewId) {
    return $http.get(API)
    .then(function(resp) {
      return self.data.member = _.find(resp.data.navigation[0].items, {"id": _.parseInt(crewId)})
    })
  }

})
